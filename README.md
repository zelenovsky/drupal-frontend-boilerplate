# Drupal Frontend Boilerplate

Webpack configuration and build scripts to deploy ES2015+ code to production (via `<script type="module">`) with legacy browser fallback support via `<script nomodule>` in Drupal.

Inspired by this article: [Deploying ES2015+ Code in Production Today](https://philipwalton.com/articles/deploying-es2015-code-in-production-today/).

## Usage

To view site locally, run the following command:

```sh
yarn dev:full
```

This will build all the source files, watch for changes, and serve them from [`http://localhost:8081`](http://localhost:8081).

To build the minified version run:

```sh
yarn prod:full
```
