const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

const plugins = require('./plugins');

const {
  getFilePath,
  getPublicPath
} = require('./paths');

const {
  styleLoader,
  babelLoader,
  imageLoader,
  fontLoader
} = require('./loaders');

const {
  publicDir
} = require('./config.json');


const isModern = process.env.BROWSERS_ENV === 'modern';
const isProd = process.env.NODE_ENV === 'production';


const configureEntry = () => {
  return {
    'app': isModern ? './app/app.js' : './app/app-legacy.js',
  }
}

const configureOutput = () => {
  return {
    crossOriginLoading: 'anonymous',
    path: path.resolve(publicDir),
    publicPath: getPublicPath(),
    filename: isProd
      ? getFilePath('js/[name].[chunkhash:10].js')
      : getFilePath('js/[name].js')
  }
}


module.exports = {
  entry: configureEntry(),
  output: configureOutput(),
  mode: isProd ? 'production' : 'development',
  watch: !isProd && isModern ? true : false,
  cache: {},
  devtool: isProd ? 'source-map' : 'inline-source-map',
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    },
    minimizer: [
      new TerserPlugin({
        test: /\.m?js$/,
        sourceMap: true,
        terserOptions: {
          safari10: true
        },
      }),
    ],
  },
  module: {
    rules: [
      babelLoader(),
      styleLoader(),
      imageLoader(),
      fontLoader()
    ],
  },
  plugins: plugins()
}
