const path = require('path');
const {
  modernBundleName,
  legacyBundleName
} = require('../config.json');

const isModern = process.env.BROWSERS_ENV === 'modern';
const prefix = isModern ? modernBundleName : legacyBundleName;

module.exports = (relativePath) => (
  path.join(prefix, relativePath)
);
