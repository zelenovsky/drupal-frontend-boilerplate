const path = require('path');


module.exports = () => {
  let prefix
  const absolutePath = path.resolve('').split(path.sep)

  absolutePath.forEach((currentDir, index) => {
    if (currentDir === 'themes') {
      prefix = absolutePath.slice(index)
      prefix = path.join(...prefix)
    }
  })

  return path.join(prefix, '/')
}