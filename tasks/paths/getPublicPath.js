const path = require('path');

const {
  publicDir
} = require('../config.json');

const getPrefix = require('./getPrefix');


module.exports = () => path.join(getPrefix(), publicDir, '/')