const getFilePath = require('./getFilePath');
const getPrefix = require('./getPrefix');
const getPublicPath = require('./getPublicPath');

module.exports = {
  getFilePath,
  getPrefix,
  getPublicPath
}