const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')
const glob = require('glob')

const {
  appStylesName,
  appModernScriptsName,
  appLegacyScriptsName
} = require('../config.json')


const appStyles = {}
const appModernScripts = {}
const appLegacyScripts = {}
let newLibraries = []


const writeCSSAsset = (href) => {
  appStyles[href] = {}
}


const writeJSAsset = (src, modern = false) => {
  modern
    ? appModernScripts[src] = { attributes: { type: 'module' } }
    : appLegacyScripts[src] = { attributes: { nomodule: true, defer: true } }
}


const writeLibraries = () => {
  const librariesPath = glob.sync(path.resolve('*.libraries.yml'))[0]
  const libraries = yaml.safeLoad(fs.readFileSync(librariesPath, 'utf8')) || {}

  newLibraries = []

  if (libraries[appStylesName] && libraries[appStylesName].css) {
    for (let weight in libraries[appStylesName].css) {

      libraries[appStylesName] = {
        css: {
          [weight]: {
            ...appStyles
          }
        }
      }
    }
  } else {

    libraries[appStylesName] = {
      css: {
        component: {
          ...appStyles
        }
      }
    }
  }
  newLibraries.push(appStylesName)


  if (libraries[appModernScriptsName] && libraries[appModernScriptsName].dependencies) {
    const dependencies = libraries[appModernScriptsName].dependencies

    libraries[appModernScriptsName] = {
      header: true,
      js: {
        ...appModernScripts
      },
      dependencies: dependencies
    }
  } else {

    libraries[appModernScriptsName] = {
      header: true,
      js: {
        ...appModernScripts
      }
    }
  }
  newLibraries.push(appModernScriptsName)


  if (libraries[appLegacyScriptsName] && libraries[appLegacyScriptsName].dependencies) {
    const dependencies = libraries[appLegacyScriptsName].dependencies

    libraries[appLegacyScriptsName] = {
      js: {
        ...appLegacyScripts
      },
      dependencies: dependencies
    }
  } else {

    libraries[appLegacyScriptsName] = {
      js: {
        ...appLegacyScripts
      }
    }
  }
  newLibraries.push(appLegacyScriptsName)


  const ws = fs.createWriteStream(librariesPath)
  const modifiedLabraries = yaml.safeDump(libraries, {
    condenseFlow: true
  })
  ws.end(modifiedLabraries)
}


const writeInfo = () => {
  const infoPath = glob.sync(path.resolve('*.info.yml'))[0]
  const info = yaml.safeLoad(fs.readFileSync(infoPath, 'utf8')) || {}
  const infoName = info.name
  const oldLibraries = info.libraries || []

  newLibraries.forEach((libriary) => {
    libriary = path.join(infoName, libriary)
    if (oldLibraries.includes(libriary)) return
    oldLibraries.push(libriary)
  })

  info.libraries = oldLibraries

  const ws = fs.createWriteStream(infoPath)
  const modifiedInfo = yaml.safeDump(info, {
    condenseFlow: true
  })
  ws.end(modifiedInfo)
}


module.exports = {
  writeCSSAsset,
  writeJSAsset,
  writeLibraries,
  writeInfo
}