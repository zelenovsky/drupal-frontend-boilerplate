const path = require('path')
const glob = require('glob')

const {
  publicDir,
  legacyBundleName,
  manifestName
} = require('../config.json')

const {
  getPrefix
} = require('../paths')

const {
  writeCSSAsset,
  writeJSAsset,
  writeLibraries,
  writeInfo
} = require('../utils/writeAssets')

const isProd = process.env.NODE_ENV === 'production';

class ModernBuildPlugin {
  apply(compiler) {
    const pluginName = 'modern-build-plugin';

    // Получаем информацию о Fallback Build
    const legacyManifest = require(path.resolve(publicDir, legacyBundleName, manifestName));

    compiler.hooks.compilation.tap(pluginName, (compilation) => {
      // Подписываемся на хук html-webpack-plugin,
      // в котором можно менять данные HTML
      compilation.hooks.htmlWebpackPluginAlterAssetTags.tapAsync(pluginName, (data, cb) => {

        data.head.forEach((tag) => {
          if (tag.tagName === 'link' && tag.attributes.rel === 'stylesheet') {
            let href = tag.attributes.href.split(getPrefix())
            href = path.join(...href)
            tag.attributes.href = href
            writeCSSAsset(href)
          }
        })

        data.body.forEach((tag) => {
          if (tag.tagName === 'script' && tag.attributes) {
            let src = tag.attributes.src.split(getPrefix())
            src = path.join(...src)

            data.head.push({
              tagName: 'script',
              closeTag: true,
              attributes: {
                type: 'module',
                src: src
              }
            })

            writeJSAsset(src, true)

            src = path.parse(src)
            const fileName = src.base
            src = legacyManifest[
                isProd ? fileName.replace(/[_\.\-][0-9a-f]{10}/, '') : fileName
            ].split(getPrefix())
            src = path.join(...src)

            tag.attributes.nomodule = true
            tag.attributes.defer = true
            tag.attributes.src = src
            delete tag.attributes.type

            writeJSAsset(src, false)
          }
        });

        if (
          glob.sync(path.resolve('*.libraries.yml'))[0]
          && glob.sync(path.resolve('*.info.yml'))[0]
        ) {
          writeLibraries();
          writeInfo();
        }

        cb();
      });
    });
  }
}

module.exports = ModernBuildPlugin;
