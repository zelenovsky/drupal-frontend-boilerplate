const path = require('path');
const webpack = require('webpack');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const ModernBuildPlugin = require('./ModernBuildPlugin');

const {
  getFilePath
} = require('../paths');

const {
  publicDir,
  manifestName,
  proxy
} = require('../config.json');

const isModern = process.env.BROWSERS_ENV === 'modern';
const isProd = process.env.NODE_ENV === 'production';


module.exports = () => {
  return [

    new MiniCssExtractPlugin({
      filename: isProd
        ? getFilePath('css/[name].[chunkhash:10].css')
        : getFilePath('css/[name].css')
    }),

    ...(isProd
      ? [
        new OptimizeCSSAssetsPlugin({
          cssProcessorOptions: {
            map: {
              inline: false,
              annotation: true,
            },
            safe: true,
            discardComments: true
          },
        })
      ]
      : []
    ),

    ...(isModern
      ? [
        new HtmlWebpackPlugin({
          filename: path.resolve(publicDir, 'index.html')
        }),
        new ModernBuildPlugin()
      ]
      : [
          new CleanWebpackPlugin(
            path.resolve(publicDir), {
              root: '/',
              verbose: false
            }
          )
      ]
    ),

    new ManifestPlugin({
      fileName: getFilePath(manifestName)
    }),

    new webpack.HashedModuleIdsPlugin(),

    ...(!isProd && isModern
      ? [
        new BrowserSyncPlugin({
          host: 'localhost',
          port: 8081,
          proxy: proxy,
          reloadDelay: 50,
          injectChanges: false,
          reloadDebounce: 500,
          reloadOnRestart: true,
        })
      ]
      : []
    )

  ];
}