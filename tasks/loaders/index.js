const babelLoader = require('./babelLoader');
const styleLoader = require('./styleLoader');
const imageLoader = require('./imageLoader');
const fontLoader = require('./fontLoader');


module.exports = {
  babelLoader,
  styleLoader,
  imageLoader,
  fontLoader
}