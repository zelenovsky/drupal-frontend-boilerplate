module.exports = () => {
  return {
    test: /\.(woff(2)?|eot|ttf|otf)$/,
    loader: 'file-loader',
    options: {
      name: 'fonts/[name].[ext]'
    }
  }
}