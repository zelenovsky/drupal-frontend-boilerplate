const isProd = process.env.NODE_ENV === 'production';

module.exports = () => {
  if (!isProd) {
    return {
      test: /\.(png|jpe?g|gif|svg|webp)$/i,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: 'images/[name].[ext]'
          }
        }
      ]
    }
  } else {
    return {
      test: /\.(png|jpe?g|gif|svg|webp)$/i,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: 'images/[name].[hash:10].[ext]'
          }
        },
        {
          loader: 'img-loader',
          options: {
            plugins: [
              require('imagemin-gifsicle')({
                interlaced: true
              }),
              require('imagemin-mozjpeg')({
                progressive: true,
                arithmetic: false
              }),
              require('imagemin-optipng')({
                optimizationLevel: 5
              }),
              require('imagemin-svgo')({
                plugins: [
                  { convertPathData: false }
                ]
              })
            ]
          }
        }
      ]
    }
  }
}